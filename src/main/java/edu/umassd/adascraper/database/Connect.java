package edu.umassd.adascraper.database;

/*Contains the methods for creating a connection to, and disconnecting from the database*/

import java.sql.*;
import java.util.concurrent.Executors;
import java.util.*;
import edu.umassd.adascraper.util.AISdata;

public class Connect {
	Connection c;
	boolean flag = true;

	public Connection dbConnect() {

		try {
			// //If accessing for the first time, use
			// 'jdbc:mysql://104.238.132.139:3306/?useSSL=false'
			// //String databaseURL =
			// "jdbc:mysql://104.238.132.139:3306/aisproject?useSSL=false";
			// String user = "aisuser";
			// String password = "aisdatabase";
			// //Open a connection to database
			// //c = DriverManager.getConnection(databaseURL, user, password);
			// try {
			// Class.forName("org.sqlite.JDBC");
			// } catch (ClassNotFoundException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// String databaseURL = "jdbc:sqlite:aisdatatable";
			// c = DriverManager.getConnection(databaseURL);
			// c.setNetworkTimeout(Executors.newFixedThreadPool(1), 1000000000);

			String databaseURL = "jdbc:mysql://76.127.170.165:3306/aisdata?user=aisdatauser&password=aisdata123&autoreconnect=true&useSSL=false";
			c = DriverManager.getConnection(databaseURL);

			if (c != null) {
				flag = false;
				System.out.println("Connected to the database");
			}
			return c;
		} catch (SQLException se) {
			System.out.println("An error occurred");
			se.printStackTrace();
			if (c == null) {
				while (flag) {
					System.out.println("Attempting to reconnect to the database");
					dbConnect();
				}
			}
			se.printStackTrace();
			return null;
		}
	}

	public Connection reconnect() {

		try {
			String databaseURL = "jdbc:mysql://76.127.170.165:3306/aisdata?user=aisdatauser&password=aisdata123&autoreconnect=true&useSSL=false";
			// Open a connection to database
			c = DriverManager.getConnection(databaseURL);
			c.setNetworkTimeout(Executors.newFixedThreadPool(1), 1000000000);
			if (c != null) {
				flag = false;
				System.out.println("Connected to the database");
			}
			return c;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	public Connection getConnection()
	{
		return c;
	}

	public void dbDisconnect(Connection c) {
		try {
			if (c != null) {
				c.close();
				System.out.println("Successfully disconnected from database!");
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}
	
	public void newEntry(String tName, AISdata ais) {
		try {
			Statement s = c.createStatement();
			String newEntry = "INSERT INTO " + tName + " " + "VALUES ('" + ais.getName() + "', '" + ais.getShipType()
					+ "', '" + ais.getFlag() + "', '" + ais.getEta() + "', '" + ais.getDestination() + "', "
					+ ais.getLat() + ", " + ais.getLon() + ", " + ais.getCourse() + ", " + ais.getSpeed() + ", "
					+ ais.getDraught() + ", '" + ais.getCallSign() + "', '" + ais.getMMSI() + "', '"
					+ ais.getLastReport() + "', '" + ais.getLastReportTime() + "', '" + ais.getIMO() + "', "
					+ ais.getLength() + ", " + ais.getWidth() + ", " + ais.getHeading() + ")";
			s.executeUpdate(newEntry);
			System.out.println("Record successfully inserted into table");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	public ArrayList<Integer> getBlockedShips() {
		ResultSet r;
		ArrayList<Integer> excludeList = new ArrayList<Integer>();
		
		try {
			Statement s = c.createStatement();
			String newEntry = "SELECT * FROM excludeship";
			r = s.executeQuery(newEntry);
			while(r.next())
			{
				excludeList.add(r.getInt("mmsi"));
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
		return excludeList;
	}
}