package edu.umassd.adascraper.scraper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.umassd.adascraper.database.Connect;
import edu.umassd.adascraper.util.AISdata;

public class Scraper1 extends Thread {
	
	Semaphore sem;
	static Connect c;
	int pgNbr = 1;
	int count = 0;
	static ArrayList<Integer> blockedShips;
	
	public Scraper1(Semaphore sem, Connect c){
		 this.sem = sem;
		 this.c = c;
	 }

	public void run(){
				
		crawl(sem);
	}
	
	public static void crawl(Semaphore sem){
		
		while(true){
			try{							
				//Connect to HTML document via URL
				Document doc = Jsoup.connect("https://www.vesselfinder.com/vessels?page=1").get();
			
				//Locate vessel chart in HTML document
				Element table = doc.select("table").get(0);
				Elements data = table.select("tr > td.v1");
			
				//Search for URL to each vessel on current page
				Elements link = data.select("a");				
				for(Element links : link){
					//Get inputs for scraper
					String url = links.attr("abs:href");					
											
					try {				
						sem.acquire();
						System.out.println("\nThread 1:\n");
						scrape(url);
						Thread.sleep(2500 + randWait(1000)); 	//Wait time
					} 
					catch (InterruptedException e) {						
						e.printStackTrace();
					}
					sem.release();
					Thread.yield();
					}			
				}
			catch (IOException e){
				e.printStackTrace();
			}
		}
	}
	
	public static void scrape(String url){
		
		blockedShips = c.getBlockedShips();
		boolean shipIsBlocked = false;		//boolean to indicate if the scraped ship is on the blocked list
		AISdata ship = new AISdata();
		
		try{			
			Document doc = Jsoup.connect(url).get();	//Connect to HTML document via URL
			
			//Get vessel name
			Elements name = doc.select("p.text1 > strong");
			ship.shipName = name.get(0).text();
			System.out.println("Name: " + ship.shipName);
		
			//Locate AIS data table in HTML document
			Element table = doc.select("table").get(0);
			Elements data = table.select("tr > td");
		
			//Output each line of AIS data chart
			for(int i=0; i < data.size(); i++) {
				String scrapedData = data.get(i).text();
				if (scrapedData.equals("-")) {
					scrapedData = "";
				}
				
				//Filter data
				if(i%2==1 && i!=3){
					
					switch(i){
					
					case 1: //Type
						ship.shipType = scrapedData;
						System.out.println("Type: " + ship.shipType);
						break;
						
					case 5: //Destination
						ship.destination = scrapedData;
						System.out.println("Dest: " + ship.destination);
						break;
						
					case 7: //ETA						
						ship.eta = scrapedData;
						System.out.println("ETA: " + ship.eta);
						break;
						
					case 9: //IMO, MMSI, and Flag						
						parseIMO(scrapedData, ship);
						
						//Check if its blocked or not
						//System.out.println("Blocked Ships:");
						for(int ships : blockedShips)
						{
							//System.out.println(Integer.toString(ships));
							if(ship.mmsi.equals(Integer.toString(ships)))
									shipIsBlocked = true;
						}
						
						System.out.println("IMO: " + ship.IMO);
						System.out.println("MMSI: " + ship.mmsi);
						System.out.println("Flag: " + ship.flag);												
						break;
						
					case 11: //Callsign
						ship.callSign = scrapedData;
						System.out.println("Callsign: " + ship.callSign);
						break;
						
					case 13: //Length/Width
						ship.setLength(scrapedData);
						ship.setWidth(scrapedData);
						System.out.println("Size: " + ship.length + " x " + ship.width);
						break;
						
					case 15: //Draught
						ship.setDraught(scrapedData);
						System.out.println("Draught: " + ship.draught);
						break;
						
					case 17: //Course and Speed
						parseSpeed(scrapedData, ship);
						System.out.println("Course: " + ship.course);
						System.out.println("Speed: " + ship.speed);
						break;
						
					case 19: //Coordinates
						parseCoords(scrapedData, ship);
						System.out.println("Lat: " + ship.lat);
						System.out.println("Lon: " + ship.lon);
						break;
						
					case 21: //Last Report Time
						ship.lastReport = parseDate(scrapedData);
						ship.lastReportTime = parseTime(scrapedData);
						System.out.println("Date and Time: " + ship.lastReport + " " + ship.lastReportTime);
						break;							
					}					
				}				
			}
			
			//if ship is blocked, dont add to database
			if(!shipIsBlocked)
				c.newEntry("aisdatatable", ship);
		}
		catch (IOException e){
			e.printStackTrace();
		}			
	}
	
	private static String parseDate(String s){
		String temp = "";
		String month = "";
		String day = "";
		String year = "";
		
		int j = 0;
		
		//Get month to be parsed
		for (int i = 0; i < 3; i++) {
			
			month += s.charAt(i);			
		}
		
		//Parse year
		for(int i = s.length()-14; j < 4; j++){
				
			year+= s.charAt(i);
			i++;
		}		
		temp += year;
		
		//Parse Month
		if(month.equals("Jan"))
			temp += "-01";	
		else if(month.equals("Feb"))
			temp += "-02";
		else if(month.equals("Mar"))
			temp += "-03";
		else if(month.equals("Apr"))
			temp += "-04";
		else if(month.equals("May"))
			temp += "-05";
		else if(month.equals("Jun"))
			temp += "-06";
		else if(month.equals("Jul"))
			temp += "-07";
		else if(month.equals("Aug"))
			temp += "-08";
		else if(month.equals("Sep"))
			temp += "-09";
		else if(month.equals("Oct"))
			temp += "-10";
		else if(month.equals("Nov"))
			temp += "-11";
		else if(month.equals("Dec"))
			temp += "-12";
		
		//Parse day
		for(int i = 4; Character.isDigit(s.charAt(i)); i++){
			
			day+= s.charAt(i);
		}
		if(day.length()==1)
			temp += "-0" + day;
		else
			temp += "-" + day;		
		
		return temp; 
	}
	
	private static String parseTime(String s){
		
		int j = 0;
		String time = "";
		
		for(int i = s.length()-9; j < 5; j++){
				
			time+= s.charAt(i);
			i++;
		}		
		time += ":00";
		
		return time;
	}
	
	private static void parseIMO(String s, AISdata ship){
		
		String temp1 = "";
		String temp2 = "";
		boolean m = true;

		for (int i = 0; i < s.length(); i++) {

			if (s.charAt(i) == ' '){
				m = false;
				i+=3;
			}
			if(m){
				if(s.charAt(i)=='-'){
					temp1 = "";
				}					
				else{
					temp1 += s.charAt(i);									
				}				
			}
			else{
				if(s.charAt(i)=='-'){
					temp2 = "";
				}					
				else{
					temp2 += s.charAt(i);									
				}	
			}	
		}
		ship.IMO = temp1;
		ship.mmsi = temp2;		
		Flag f = new Flag();
		ship.flag = f.getFlag(ship.mmsi);				
	}
	
	private static void parseSpeed(String s, AISdata ship){
		
		String temp1 = "";
		String temp2 = "";
		boolean m = true;
		
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == ' '){
				m = false;
				i+=2;
			}
			if(m){
				if(s.charAt(i)=='-'){
					temp1 = "";
				}					
				else{
					if(Character.isDigit(s.charAt(i)))
						temp1 += s.charAt(i);									
				}				
			}
			else{
				if(s.charAt(i)=='-'){
					temp2 = "";
				}					
				else{
					if(Character.isDigit(s.charAt(i)))
						temp2 += s.charAt(i);									
				}	
			}	
		}

		double t1 = 0.0;
		double t2 = 0.0;
		
		if(temp1 != "")
			t1 = Double.parseDouble(temp1)/10;
		else
			t1 = -1.0;
		
		if(temp2 != "")
			t2 = Double.parseDouble(temp2)/10;
		else
			t2 = -1.0;
		
		ship.course = t1;
		ship.speed = t2;	
	}
	
	private static void parseCoords(String s, AISdata ship){
		
		String temp1 = "";
		String temp2 = "";
		boolean m = true;

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == ' '){
				m = false;
				i++;
			}
			if(m){
				if(s.charAt(i)=='-'){
					temp1 = "";
				}					
				else{
					if(Character.isDigit(s.charAt(i)))
						temp1 += s.charAt(i);									
				}				
			}
			else{
				if(s.charAt(i)=='-'){
					temp2 = "";
				}					
				else{
					if(Character.isDigit(s.charAt(i)))
						temp2 += s.charAt(i);									
				}	
			}	
		}
		
		double t1 = Double.parseDouble(temp1)/100000;
		double t2 = Double.parseDouble(temp2)/100000;		
		ship.lat = t1;
		ship.lon = t2;		
	}
	
	private static int randWait(int max) {
		
		Random r = new Random();
		return r.nextInt(max);
	}
}
