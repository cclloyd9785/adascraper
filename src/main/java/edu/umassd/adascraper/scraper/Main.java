package edu.umassd.adascraper.scraper;

import java.util.concurrent.Semaphore;
import edu.umassd.adascraper.database.Connect;

public class Main {

	public static void main(String[] args) throws InterruptedException{
		
		Connect c = new Connect();
		c.dbConnect();
		
		Semaphore sem = new Semaphore(1);
		
		Scraper1 s1 = new Scraper1(sem, c);
		Scraper2 s2 = new Scraper2(sem, c);
		
		//Start the scraper processes		
		s1.start();		
		s2.start();
				
		//s1.join();
		//s2.join();
	}
}
