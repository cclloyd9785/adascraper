package edu.umassd.adascraper.scraper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.umassd.adascraper.util.AISdata;
import edu.umassd.adascraper.database.Connect;

public class Scraper2 extends Thread {
	
	Semaphore sem;
	static Connect c;
	int pgNbr = 1;
	int count = 0;
	static ArrayList<Integer> blockedShips;
	
	public Scraper2(Semaphore sem , Connect c){
		 this.sem = sem;
		 this.c = c;
	 }

	public void run(){				
		
		crawl(sem);
	}
	
	public static void crawl(Semaphore sem){
		
		while(true){
			try{						
				//Connect to HTML document via URL
				Document doc = Jsoup.connect("http://www.myshiptracking.com/search/vessels&page=1").get();
		
				//Locate vessel chart in HTML document
				Elements data = doc.select(".table_main .table_title");
		
				//Search for URL to each vessel on current page
				Elements link = data.select("a");				
				for(Element links : link){
					//Get inputs for scraper
					String url = links.attr("abs:href");
					
					try {				
						sem.acquire();
						System.out.println("\nThread 2:\n");
						scrape(url);
						Thread.sleep(2500 + randWait(1000)); 	//Wait time
					} 
					catch (InterruptedException e) {
						e.printStackTrace();
					}								
					sem.release();
					Thread.yield();
				}			
			}
			catch (IOException e){
			e.printStackTrace();
			}
		}
	}			

	public static void scrape(String url){
		
		blockedShips = c.getBlockedShips();
		boolean shipIsBlocked = false;		//boolean to indicate if the scraped ship is on the blocked list
		AISdata ship = new AISdata();
	
		try{			
			Document doc = Jsoup.connect(url).get();	//Connect to HTML document via URL
		
			//Locate AIS data charts in HTML document		
			for(int i=0; i<=2; i++){
			
				Element table = doc.select("table").get(i);
				Elements data = table.select("tr > td"); 
				
				//Output each line of current AIS data chart
				for(int j=0; j < data.size(); j++) {
					
					String scrapedData = data.get(j).text();
					if (scrapedData.equals("---")) {
						scrapedData = "";
					}
					
					//Filter data
					switch (i) {
					case 0:
						switch (j) {
						case 1: //Name
							ship.shipName = scrapedData;
							System.out.println("Name: " + ship.shipName);
							break;
							
						case 5: //MMSI and Flag
							Flag f = new Flag();
							ship.mmsi = scrapedData;
							
							//Check if its blocked or not
							//System.out.println("Blocked Ships:");
							for(int ships : blockedShips)
							{
								//System.out.println(Integer.toString(ships));
								if(ship.mmsi.equals(Integer.toString(ships)))
										shipIsBlocked = true;
							}
							
							ship.flag = f.getFlag(ship.mmsi);
							System.out.println("MMSI: " + ship.mmsi);
							System.out.println("Flag: " + ship.flag);
							break;
							
						case 7: //IMO
							ship.IMO = scrapedData;
							System.out.println("IMO: " + ship.IMO);
							break;
							
						case 9: //Callsign
							ship.callSign = scrapedData;
							System.out.println("Callsign: " + ship.callSign);
							break;
							
						case 11: //Type
							ship.shipType = scrapedData;
							System.out.println("Type: " + ship.shipType);
							break;
							
						case 13: //Size
							// parse size
							ship.setLength(scrapedData);
							ship.setWidth(scrapedData);
							System.out.println("Size: " + ship.length + " x " + ship.width);
							break;
							
						default:
							// skip
							break;
						}
						break;

					case 1:
						switch (j) {
						case 1: //Longitude
							ship.lon = Double.parseDouble(scrapedData.substring(0, scrapedData.length() - 2));
							System.out.println("Lon: " + ship.lon);
							break;
							
						case 3: //Latitude
							ship.lat = Double.parseDouble(scrapedData.substring(0, scrapedData.length() - 2));
							System.out.println("Lat: " + ship.lat);
							break;
							
						case 7: //Speed		
							ship.setSpeed(scrapedData);
							System.out.println("Speed: " + ship.speed);
							break;
							
						case 9: //Course
							ship.setCourse(scrapedData);
							System.out.println("Course: " + ship.course);
							break;

						default:
							// skip
							break;
						}
						break;

					case 2:
						switch (j) {
						case 3: //Destination
							ship.destination = scrapedData;
							System.out.println("Dest: " + ship.destination);
							break;
							
						case 5: //Eta
							ship.eta = scrapedData;
							System.out.println("ETA: " + ship.eta);
							break;
							
						case 7: //Draught
							ship.setDraught(scrapedData);
							System.out.println("Draught: " + ship.draught);
							break;
							
						case 9: //Last Report Time
							if(scrapedData != ""){
								ship.lastReport = parseDate(scrapedData);
								ship.lastReportTime = parseTime(scrapedData);
							}						
							System.out.println("Date and Time: " + ship.lastReport + " " + ship.lastReportTime);
							break;
							
						default:
							// skip
							break;
						}
						break;
					}
				}
			}
			
			//if ship is blocked, dont add to database
			if(!shipIsBlocked)
				c.newEntry("aisdatatable", ship);
		} 
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static String parseDate(String in) {
		String temp = "";

		for (int i = 0; i < in.length(); i++) {
			temp += in.charAt(i);
			if (in.charAt(i + 1) == ' ') {
				i = in.length();
			}
		}

		return temp;
	}

	private static String parseTime(String in) {
		String temp = "";
		Boolean start = false;

		for (int i = 0; i < in.length(); i++) {
			if (start)
				temp += in.charAt(i);

			if (in.charAt(i) == ' ') {
				start = true;
			}
		}
		temp += ":00";

		return temp;
	}

	private static int randWait(int max) {
		
		Random r = new Random();
		return r.nextInt(max);
	}
			
}
